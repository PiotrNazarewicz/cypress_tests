# Cypress Code Challenge

This repo is a starter kit for the QA Engineer code challenge. Follow the instructions below to download and set up a cypress project.

### Prerequisites

- Git
- Node and npm installed on your system (yarn optional)

### Setup

1. Use git to clone this repo locally (`git clone https://PiotrNazarewicz@bitbucket.org/PiotrNazarewicz/cypress_tests.git`)
2. Run `yarn` or `npm install`
3. Run the example test with `yarn test` or `npm test` to verify that things are working correctly

If everything is working correctly you should see a message like

```
      Spec                                                Tests  Passing  Failing  Pending  Skipped 
  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ ✔ example_spec.js                  00:04        1        1        -        -        - │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘
    All specs passed!                           00:04        1        1        -        -        -  
```

4. Once you see the above message, you are ready!


### Resources

- [Cypress Docs](https://docs.cypress.io/guides/overview/why-cypress.html#In-a-nutshell)
